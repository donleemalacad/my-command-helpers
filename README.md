# My Bash Aliases

### Steps to Build
```
# Add following commands to this file
vi ~/.bashrc
source ~/.bashrc
```

### Shortcut Command Reference:
- *dockstart* - Start docker with specific services (nginx, redis)
- *dockdown* - Stop docker instances
- *dockstat* - Show running containers
- *docklogs* - Get logs for specific container
- *dockbuild* - Specific image docker build
- *dockbuildnc* - Specific image docker build no cache
- *dockspec* - Specific docker image run up
- *dockexec* - Specific docker container execution
- *dockres* - Restart docker
- *gstat* - Check status of git
- *gadd* - Add files to git
- *gcommit* - Commit files changed to branch
- *gnbranch* - Checkout to new branch
- *gbranch* - Checkout to existing branch
- *t* - PHPUnit testing
- *tfil* - PHPUnit testing for a specific file
- *compdev* - Compile assets for dev environment
- *back* - Go back 1 step
- *mapcount* - In case elasticsearch fails, Increase count